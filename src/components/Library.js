import React from 'react';
import { LibrarySong } from './LibrarySong';

export const Library = ({ songs, setCurrentSong, audioRef, isPlaying, setSongs, librarySatus }) => {
   return (
      <div className={`library ${librarySatus ? 'active-library ' : ''}`}>
         <h2>Library</h2>
         <div className="library-songs">
            {songs.map((song) => (
               <LibrarySong
                  id={song.id}
                  songs={songs}
                  setCurrentSong={setCurrentSong}
                  song={song}
                  key={song.id}
                  audioRef={audioRef}
                  isPlaying={isPlaying}
                  setSongs={setSongs}
               />
            ))}
         </div>
      </div>
   );
};
