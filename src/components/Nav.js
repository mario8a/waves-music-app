import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faMusic
} from "@fortawesome/free-solid-svg-icons";

export const Nav = ({librarySatus, setLibrarySatus}) => {

  return (
    <nav>
      <h1>Waves</h1>
      <button onClick={() => setLibrarySatus(!librarySatus)}>
        Library
        <FontAwesomeIcon icon={faMusic} />
      </button>
    </nav>
  )
};
